
# Spring Boot app with pipeline
Empty Spring Boot application deployed with gitlab-ci pipeline to heroku on a docker image.


## Environment Variables
To run this project, you will need to add the following environment variables to your .env file

`HEROKU_API_KEY`
The heroku required authentication token for deployment purpuse.

    # To get this token, first login to heroku CLI
    heroku login -i
    # Then you can get the token with this command :
    heroku authorizations:create

`PORT`
The port to start the Spring Boot server on. In a local environement,
you can set it with Intellij configurations to *8080* to follow the standard.
In deployment, Heroku will provide this variable to decide witch port will be used.


## Deployment
The application is automatically built after each commit in main branch.
A *gitlab-ci.yml* file is present to configured a pipepline to build the jar with **maven**, 
build and push the **Docker** image to **Heroku** PaaS for free.


## Note
With the free plan, after 30 minutes of application unused, heroku stop the container.
The next request take few additional seconds the time to restart it.

💡 To prevent this, the app is registered on https://kaffeine.herokuapp.com.

🧠 A ping is made every 30 minutes to not have the application sleeping (still have a sleep time 01h-07h).

## Demo
https://spring-boot-app-with-pipeline.herokuapp.com


## Authors
[@antoine_blin](https://gitlab.com/antoine_blin)
