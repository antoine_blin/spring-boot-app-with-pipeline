package com.antoinedeveloperbreton.springbootappwithpipeline.services.impl;

import com.antoinedeveloperbreton.springbootappwithpipeline.models.I18nMessage;
import com.antoinedeveloperbreton.springbootappwithpipeline.models.Language;
import com.antoinedeveloperbreton.springbootappwithpipeline.services.HelloWorldService;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Random;

@Service
public class HelloWorldServiceImpl implements HelloWorldService {

    final Random random = new Random();

    private final Map<Language, String> helloWorldsByLanguage = Map.of(
            Language.EN, "Hello World",
            Language.FR, "Bonjour le monde",
            Language.IT, "Ciao mondo");

    @Override
    public I18nMessage getHelloWorldMessageInRandomLanguage() {
        final Language[] languages = helloWorldsByLanguage.keySet().toArray(new Language[0]);
        final int randomLanguageIndex = random.nextInt(languages.length);

        final Language randomLanguage = languages[randomLanguageIndex];
        final String helloWorldMessageInRandomLanguage = helloWorldsByLanguage.get(languages[randomLanguageIndex]);

        return new I18nMessage(helloWorldMessageInRandomLanguage, randomLanguage);
    }
}
