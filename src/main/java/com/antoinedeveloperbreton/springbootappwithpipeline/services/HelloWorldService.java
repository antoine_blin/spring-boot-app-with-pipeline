package com.antoinedeveloperbreton.springbootappwithpipeline.services;

import com.antoinedeveloperbreton.springbootappwithpipeline.models.I18nMessage;

public interface HelloWorldService {

    I18nMessage getHelloWorldMessageInRandomLanguage();

}
