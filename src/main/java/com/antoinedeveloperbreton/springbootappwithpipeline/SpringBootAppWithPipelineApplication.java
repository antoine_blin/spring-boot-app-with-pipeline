package com.antoinedeveloperbreton.springbootappwithpipeline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAppWithPipelineApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootAppWithPipelineApplication.class, args);
    }

}
