package com.antoinedeveloperbreton.springbootappwithpipeline.models;

public class I18nMessage {

    private String message;
    private Language language;

    public I18nMessage(String message, Language language) {
        this.message = message;
        this.language = language;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}
