package com.antoinedeveloperbreton.springbootappwithpipeline.models;

public enum Language {
    EN,
    FR,
    IT
}
