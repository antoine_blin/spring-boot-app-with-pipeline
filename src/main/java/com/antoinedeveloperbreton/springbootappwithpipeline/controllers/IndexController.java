package com.antoinedeveloperbreton.springbootappwithpipeline.controllers;

import com.antoinedeveloperbreton.springbootappwithpipeline.models.I18nMessage;
import com.antoinedeveloperbreton.springbootappwithpipeline.services.HelloWorldService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

    private final HelloWorldService helloWorldService;

    public IndexController(HelloWorldService helloWorldService) {
        this.helloWorldService = helloWorldService;
    }

    /**
     * @return a I18Message object with and Hello World message in a random language
     */
    @GetMapping("/")
    public I18nMessage index() {
        return helloWorldService.getHelloWorldMessageInRandomLanguage();
    }
}
