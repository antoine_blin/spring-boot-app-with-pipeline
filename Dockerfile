FROM openjdk:13-jdk-alpine
MAINTAINER antoine.blin.perso@gmail.com
COPY target/*.jar /app.jar
ENTRYPOINT ["java","-jar","/app.jar"]